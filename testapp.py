import pytest
import json

from app import app
from app import Water

@pytest.fixture()
def defapp():
    app.config.update({
        "TESTING": True,
    })
    yield app


@pytest.fixture()
def client(defapp):
    return defapp.test_client()


# def test_read_water(client):
    # response = client.get("/water")
    # result = json.loads(response.data)
    # assert 70 == result['water']
    
# def test_water_class_default_value():
    # water_class =Water()
    # assert  water_class.water_value == 0
    
def test_save_water(client):
    response = client.post("/add_water/")
    result = json.loads(response.data)
    assert 80 == result['water']

# def test_save_water_for_user(client):
    # response = client.post("/add_water/1")
    # result = json.loads(response.data)
    # assert 90 == result['water']
