# WATER

## Présentation

Water est une application de suivi de consommation d'eau.

A chaque clic, cela signifie qu'un verre d'eau a été consommé.
Les informations sont sauvegardées dans un fichier au format JSON.
Il est possible de fournir un récapitulatif par jour de la consommation d'eau.
A la mi journée, il faut une alerte si le seuil minimal n'est pas atteint à 50%.

## Compilation

### Local

Pour préparer l'environnement, vous devez créer un environnement virtuel et l'activer

```bash
python -m venv waterenv
source ./waterenv/bin/activate
```

Pour lancer les tests et l'analyse de couverture de code au format XML, utilisez le script build.sh.

```bash
./build.sh
```

### Environnement Docker

Pour lancer les tests et l'analyse de couverture de code au format XML, sans avoir besoin de créer un environnement virtuel, reprenez la commande ci-après en remplaçant le chemin HOME_PROJECT/project-tofix par le chemin absolu vers le projet.

```bash
docker run --rm \
    -v HOME_PROJECT/project-tofix:/usr/src \
    -w /usr/src \
    python:3.11 ./build.sh
```


Envoyer l'analyse de code à une instance SonarQube en local.

```bash
docker run --rm -v HOME_PROJECT/project-tofix:/usr/src sonarsource/sonar-scanner-cli:latest  sonar-scanner \
  -Dsonar.projectKey=project-tofix \
  -Dsonar.host.url=http://172.17.0.2:9000 \
  -Dsonar.token=sqp_190c6dc9d29af721e10e26fc9073fa2121a32f43
```

## Utilisation

Pour utiliser et tester en situation nominal le service, utilisez les commandes suivantes.

```bash
pip install -r requirements.txt
python app.py
```

